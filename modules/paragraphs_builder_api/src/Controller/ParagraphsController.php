<?php

namespace Drupal\paragraphs_builder_api\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ParagraphsController.
 */
class ParagraphsController extends ParagraphsBuilderControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->renderer = $container->get('renderer');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * Creates a paragraph.
   *
   * @param string $type
   *   The paragraph type.
   *
   * @return \Drupal\paragraphs\ParagraphInterface
   */
  private function createParagraphEntity($type) {
    return $this->entityTypeManager->getStorage('paragraph')->create([
      'type' => $type,
    ]);
  }

  /**
   * Gets the 'paragraphs_builder' form display of a paragraph type
   *
   * @param string $type
   *   The paragraph type.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  private function getFormDiplay($type) {
    return $this->entityTypeManager->getStorage('entity_form_display')->load("paragraph.$type.paragraphs_builder");
  }

  /**
   * Get all paragraph types.
   *
   * @return JsonResponse
   *   Paragraph types.
   */
  public function getParagraphTypes() {

    $data = [];

    // Get all paragraph types.
    /** @var \Drupal\paragraphs\ParagraphsTypeInterface[] */
    $paragraph_types = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();
    // Check the access of each paragraph type.
    foreach ($paragraph_types as $paragraph_type) {
      if ($this->entityTypeManager->getAccessControlHandler('paragraph')->createAccess($paragraph_type->id(), $this->currentUser, [], true)) {
        $data[] = [
          'name' => $paragraph_type->label(),
          'id' => $paragraph_type->id(),
          'src' => $paragraph_type->getIconUrl(),
        ];
      }
    }
    return new JsonResponse($data);
  }

  public function getParagraphs($entity_type, $entity_id, $field) {
    if ($entity = $this->checkParagraphFieldInEntity($entity_type, $entity_id, $field)) {
      $build = [];

      /** @var \Drupal\paragraphs\ParagraphInterface[] */
      $paragraphs = $entity->get($field)->referencedEntities();
      foreach ($paragraphs as $paragraph) {
        if ($paragraph->access('view')) {
          $build[] = [
            'id' => $paragraph->id(),
            'revision_id' => $paragraph->getRevisionId(),
            'html' => $this->renderer->render($this->entityTypeManager->getViewBuilder('paragraph')->view($paragraph)),
          ];
        }
      }

      return new JsonResponse($build);

    } else {
      throw new NotFoundHttpException();
    }
  }

  public function saveParagraphs($entity_type, $entity_id, $field) {
    $paragraphs = $this->request->request->get('paragraphs');
    $entity = $this->checkParagraphFieldInEntity($entity_type, $entity_id, $field);
    $entity->get($field)->setValue($paragraphs);
    $entity->save();

    $this->messenger->addStatus("Paragraphs have been saved.");

    return new JsonResponse($paragraphs);
  }

  public function getParagraphsForm($type) {
    // Get the 'paragraphs_builder' form display.
    $form_display = $this->getFormDiplay($type);

    if (!isset($form_display)) {
      throw new NotFoundHttpException();
    }
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] */
    $field_definitions = $form_display->get('fieldDefinitions');

    $content = $form_display->get('content');

    foreach ($content as $field_name => &$field_info) {
      // Get some other info of the field.
      $field_definition = $field_definitions[$field_name];

      $field_info['label'] = $field_definition->getLabel();
      $field_info['required'] = $field_definition->isRequired();
    }

    return new JsonResponse($content);
  }

  public function createParagraph($entity_type, $entity_id, $field, $type) {
    if ($entity = $this->checkParagraphFieldInEntity($entity_type, $entity_id, $field)) {
      // Check if he has access.
      if (!$entity->access('update')) {
        throw new AccessDeniedHttpException();
      }

      // Check if the paragraph type exists.
      $paragraph_type = $this->entityTypeManager->getStorage('paragraphs_type')->load($type);
      if (!isset($paragraph_type)) {
        throw new NotFoundHttpException();
      }

      // Get the content of the form display.
      $content = $this->getFormDiplay($type)->get('content');

      // Begin creating the paragraph.
      $paragraph = $this->createParagraphEntity($type);

      foreach ($this->request->request->all() as $field_name => $field_value) {
        // If the field is not in the 'paragraphs_builder' form_display, we throw an access denied.
        if (!in_array($field_name, array_keys($content))) {
          throw new AccessDeniedHttpException(sprintf('You do not have access to field "%s"', $field_name));
        }

        // Else, we set the value.
        $paragraph->set($field_name, $field_value);
      }

      // Save it and return the data.
      $paragraph->save();

      // Render as html.
      // TODO: Use the view mode of the paragraphs field.
      $html = $this->renderer->render($this->entityTypeManager->getViewBuilder('paragraph')->view($paragraph));

      return new JsonResponse([
        'id' => $paragraph->id(),
        'revision_id' => $paragraph->getRevisionId(),
        'html' => $html,
        'paragraphType' => $type,
      ]);

    }
  }

}
