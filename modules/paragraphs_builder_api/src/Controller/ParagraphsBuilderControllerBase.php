<?php

namespace Drupal\paragraphs_builder_api\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class ParagraphsBuilderControllerBase.
 */
class ParagraphsBuilderControllerBase extends ControllerBase {

  /**
   * The Drupal request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $json_post = json_decode(file_get_contents('php://input'), true);
    if (is_array($json_post)) {
      $instance->request->request = new ParameterBag(json_decode(file_get_contents('php://input'), true));
    }
    return $instance;
  }

  /**
   * Checks if the entity is valid and has a pargraph field.
   *
   * @param string $entity_type
   *    The entity type.
   * @param string $entity_id
   *    The entity id.
   * @param string $field
   *    The paragraph field name.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|FALSE
   *    The entity if it is valid. FALSE otherwise.
   */
  protected function checkParagraphFieldInEntity($entity_type, $entity_id, $field) {
    // Load the entity.
    try {
      /** @var \Drupal\Core\Entity\FieldableEntityInterface */
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    } catch (PluginNotFoundException $e) {
      return FALSE;
    }

    if ($entity && $entity->hasField($field)) {

      $type = $entity->getFields()[$field]->getFieldDefinition()->getType();
      // Check if the field is a paragraph field.
      if ($type !== "entity_reference_revisions") {
        return FALSE;
      } else {
        return $entity;
      }
    }

    return FALSE;
  }
}