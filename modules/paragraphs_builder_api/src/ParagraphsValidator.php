<?php

namespace Drupal\paragraphs_builder_api;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ParagraphsValidator.
 */
class ParagraphsValidator implements ParagraphsValidatorInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ParagraphsValidator object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $account) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $account;
  }

  /**
  * {@inheritDoc}
  */
  public function validate($entity_type_id, $entity_id, $field) {
    $entity = NULL;

    // Check if the user has the permission to use paragraphs builder.
    if (!$this->currentUser->hasPermission('use paragraphs builder')) {
      throw new AccessDeniedHttpException('You don\'t have the permission to use paragraphs builder');
    }

    // Check if the entity exists.
    try {
      /** @var \Drupal\Core\Entity\FieldableEntityInterface */
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      if (!isset($entity)) {
        throw new NotFoundHttpException('Entity not found');
      }
    } catch (PluginException $e) {
      throw new NotFoundHttpException('Entity type not found');
    }

    // Check if the field is a paragarphs field.
    $field_definition = $entity->getFieldDefinition($field);
    if (!isset($field_definition) || $field_definition->getType() != 'entity_reference_revisions') {
      throw new NotFoundHttpException('Invalid paragraphs field');
    }

    // Success, we return the entity.
    return $entity;
  }

}
