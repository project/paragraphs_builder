<?php

namespace Drupal\paragraphs_builder_api;

use InvalidArgumentException;

trait ParagraphsBuilderFormTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Gets the 'paragraphs_builder' form display.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   *   The entity form display.
   */
  private function getFormDisplay($entity_type_id, $bundle) {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface */
    $form_display = $this->entityTypeManager->getStorage('entity_form_display')->load("$entity_type_id.$bundle.paragraphs_builder");

    // Validate the form display.
    if (!isset($form_display)) {
      throw new InvalidArgumentException(sprintf('The entity type "%s" with the bundle "%s" doesn\'t have "paragraphs_builder" form display'
        , [$entity_type_id, $bundle]));
    }

    return $form_display;
  }

  /**
   * Gets the 'paragraphs_builder' form display settings.
   *
   * @param \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display
   *   The form display.
   *
   * @return array
   *   An array of form display settings.
   */
  private function getFormSettings(\Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display) {

    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] */
    $field_definitions = $form_display->get('fieldDefinitions');
    $content = $form_display->get('content');

    foreach ($content as $field_name => &$field_info) {
      // Get the required and the label of each field.
      $field_definition = $field_definitions[$field_name];
      $field_info['label'] = $field_definition->getLabel();
      $field_info['required'] = $field_definition->isRequired();
    }

    return $content;
  }
}