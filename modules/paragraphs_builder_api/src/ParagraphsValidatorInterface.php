<?php

namespace Drupal\paragraphs_builder_api;

/**
 * Interface ParagraphsValidatorInterface.
 */
interface ParagraphsValidatorInterface {

  /**
   * Checks for valid permissions. And check if the field in the entity is valid. If it is, return the entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param integer $entity_id
   *   The entity id.
   * @param string $field
   *   The field.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function validate($entity_type_id, $entity_id, $field);

}
