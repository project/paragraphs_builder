<?php

namespace Drupal\paragraphs_builder_api\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\paragraphs_builder_api\ParagraphsBuilderFormTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use LogicException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "paragraphs_builder_resource",
 *   label = @Translation("Paragraphs builder resource"),
 *   uri_paths = {
 *     "canonical" = "/paragraphs_builder/{entity_type_id}/{entity_id}/{field}",
 *     "https://www.drupal.org/link-relations/create" = "/paragraphs_builder/{entity_type_id}/{entity_id}/{field}/{operation}",
 *   }
 * )
 */
class ParagraphsBuilderResource extends ResourceBase {

  use ParagraphsBuilderFormTrait;

  // GET Operations
  const GET_PARAGRAPH_TYPES = 'getParagraphTypes';
  const GET_PARAGRAPHS = 'getParagraphs';
  const GET_PARAGRAPH_FORM = 'getParagraphForm';

  // POST Operations
  const POST_SAVE_PARAGRAPHS = 'saveParagraphs';

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The paragraphs builder validator.
   *
   * @var \Drupal\paragraphs_builder_api\ParagraphsValidatorInterface
   */
  protected $paragraphsBuilderValidator;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The renderer
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('paragraphs_builder_api');
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->paragraphsBuilderValidator = $container->get('paragraphs_builder_api.validator');
    $instance->currentRequest = $container->get('request_stack')->getCurrentRequest();
    $instance->renderer = $container->get('renderer');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function permissions() {
    // We will implement our permissions.
    return [];
  }

  /**
   * Gets the current request operation.
   *
   * @return string
   *   The operation.
   */
  private function getOperation() {
    return $this->currentRequest->query->get('operation');
  }

  /**
   * Gets the the targeted paragraph types of a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   An array of paragraph type ids.
   */
  private function getParagraphTypesOfField(\Drupal\Core\Field\FieldDefinitionInterface $field_definition) {
    return $field_definition->getSetting('handler_settings')['target_bundles'];
  }

  /**
   * Gets the paragraph types.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   *
   * @param string $field
   *   The paragraphs field.
   *
   * @return ResourceResponse
   *   The paragraph types.
   */
  public function getParagraphTypes(\Drupal\Core\Entity\FieldableEntityInterface $entity, $field) {
    // Get the allowed paragraph types from the field settings.
    $target_paragraph_types = $this->getParagraphTypesOfField($entity->getFieldDefinition($field));
    $cache = new CacheableMetadata();
    $paragraph_types = [];
    if (empty($target_paragraph_types)) {
      throw new LogicException('The field must specify the allowed paragraph types');
    }

    foreach ($target_paragraph_types as $paragraph_type_id) {
      // Load the paragraph type.
      /** @var \Drupal\paragraphs\ParagraphsTypeInterface */
      $paragraph_type = $this->entityTypeManager->getStorage('paragraphs_type')->load($paragraph_type_id);
      $paragraph_types[] = [
        'id' => $paragraph_type->id(),
        'name' => $paragraph_type->label(),
        'src' => $paragraph_type->getIconUrl(),
      ];

      // The response cache should depend on these paragraph types.
      $cache->addCacheableDependency($paragraph_type);
    }

    // Add the field definition as cache dependency.
    $cache->addCacheableDependency($entity->getFieldDefinition($field));

    $response = new ResourceResponse($paragraph_types);
    $response->getCacheableMetadata()->addCacheableDependency($cache);
    return $response;
  }

  /**
   * Gets the paragraphs of an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   *
   * @param string $field
   *   The paragraphs field.
   *
   * @return ResourceResponse
   *   The paragraph types.
   */
  public function getParagraphs(\Drupal\Core\Entity\FieldableEntityInterface $entity, $field) {
    $data = [];
    $cache = new CacheableMetadata();

    /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList */
    $field_object = $entity->get($field);
    /** @var \Drupal\paragraphs\ParagraphInterface[] */
    $paragraphs = $field_object->referencedEntities();

    // Render each paragraph.
    $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');
    foreach ($paragraphs as $paragraph) {
      // Check the access.
      if ($paragraph->access('view')) {
        $data[] = [
          'id' => $paragraph->id(),
          'revision_id' => $paragraph->getRevisionId(),
          /** @todo Get the view mode from the field settings */
          'html' => $this->renderer->renderRoot($view_builder->view($paragraph)),
        ];

        // Cache dependency.
        $cache->addCacheableDependency($paragraph);
      }
    }

    // Should depend on the parent entity.
    $cache->addCacheableDependency($entity);

    $response = new ResourceResponse($data);
    $response->addCacheableDependency($cache);
    return $response;
  }

  /**
   * Checks if the response is alright.
   *
   * @param ResourceResponse $response
   *   The response.
   *
   * @return ResourceResponse
   */
  private function checkResponse($response) {
    if ($response) {
      $cache = $response->getCacheableMetadata();
      $cache->setCacheContexts(['url']);
      return $response;
    } else {
      throw new BadRequestHttpException('Invalid operation');
    }
  }

  /**
   * Gets the form display of a paragraph type.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   * @param string $field
   *   The paragraphs field.
   * @param string $paragraph_type_id
   *   The paragraph_type_id.
   *
   * @return ResourceResponse
   *   The paragraph types.
   */
  public function getParagraphForm(\Drupal\Core\Entity\FieldableEntityInterface $entity, $field, $paragraph_type_id) {
    $target_paragraph_types = $this->getParagraphTypesOfField($entity->getFieldDefinition($field));

    // Check if the the paragraph type is allowed in the field.
    if (!in_array($paragraph_type_id, $target_paragraph_types)) {
      throw new AccessDeniedHttpException('This paragraph type is not allowed');
    }

    // Get the form display settings.
    $form_display = $this->getFormDisplay('paragraph', $paragraph_type_id);
    $response = new ResourceResponse($this->getFormSettings($form_display));
    // Cachable dependency.
    $response->addCacheableDependency($form_display);

    return $response;
  }

  /**
   * Responds to GET requests.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param integer $entity_id
   *   The entity id.
   * @param string $field
   *   The field.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   */
  public function get($entity_type_id, $entity_id, $field) {
    $entity = $this->paragraphsBuilderValidator->validate($entity_type_id, $entity_id, $field);

    $response = NULL;
    switch ($this->getOperation()) {
    case self::GET_PARAGRAPH_TYPES:
      $response = $this->getParagraphTypes($entity, $field);
      break;
    case self::GET_PARAGRAPHS:
      $response = $this->getParagraphs($entity, $field);
      break;
    case self::GET_PARAGRAPH_FORM:
      $paragraph_type_id = $this->currentRequest->query->get('paragraphType');
      $response = $this->getParagraphForm($entity, $field, $paragraph_type_id);
      break;
    }

    return $this->checkResponse($response);
  }

  /**
   * Saves the paragraphs.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The fieldable entity.
   * @param string $field
   *   The paragraphs field.
   * @param array $paragraphs
   *   An array of sorted paragraphs ids.
   *
   * @return ResourceResponse
   *   The paragraph types.
   */
  public function saveParagraphs(\Drupal\Core\Entity\FieldableEntityInterface $entity, $field, array $paragraphs) {
    $entity->get($field)->setValue($paragraphs);
    $entity->save();
    $this->messenger->addStatus("Paragraphs have been saved.");
    return new ResourceResponse($paragraphs);
  }

  /**
   * Responds to POST requests.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param integer $entity_id
   *   The entity id.
   * @param string $field
   *   The field.
   * @param string $operation
   *   The operation.
   * @param array $data
   *   The data.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   */
  public function post($entity_type_id, $entity_id, $field, $operation, array $data) {
    $entity = $this->paragraphsBuilderValidator->validate($entity_type_id, $entity_id, $field);

    switch ($operation) {
    case self::POST_SAVE_PARAGRAPHS:
      return $this->saveParagraphs($entity, $field, $data);
    }
  }

}
