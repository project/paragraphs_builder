var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  mode: "development",
  entry: "./js/index.tsx",
  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    modules: ['node_modules', 'js']
  },
  plugins: [
    new LiveReloadPlugin()
  ],
  externals: {
    jquery: 'jQuery',
    lodash: '_'
  },
  module: {
    rules: [{
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [{
          loader: "ts-loader"
        }]
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: ['./sass']
              }
            }
          }
        ],
      },
    ]
  }
};
