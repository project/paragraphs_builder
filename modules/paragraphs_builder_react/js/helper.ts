import { map, keys } from 'lodash';
import { GenericObject } from './interfaces';
import Axios from 'axios';
import { ParagraphFieldComponent } from 'interfaces';
import { mainResourceGetUri, mainResourcePostUri, OPERATIONS } from 'api';

export const TYPES = {
    PARAGRAPH_TYPE: "paragraphs_type",
    PARAGRAPH: "paragraph"
}

export const getHost = () => document.getElementById('paragraphs-builder').getAttribute('data-host');


let token: string = null;
const fetchToken = async () => {
    const res = await Axios.get(getHost() + '/session/token');
    token = res.data;
}
fetchToken();

const buildUrl = (url: string, routeArguments: (string | number)[]) => {
    let urlFragments = url.split('%');
    let routedUrl = url;
    if (routeArguments.length) {
        if (routeArguments.length + 1 != urlFragments.length) {
            throw new Error("fetchData: Insufficient arguments.")
        }
        routedUrl = urlFragments.shift();
        urlFragments.forEach((urlFragment, index) => {
            routedUrl += routeArguments[index] + urlFragment.toString();
        });
    }
    return `${routedUrl}?_format=json`;
}

const getEntityData = () => {
    let element = document.getElementById('paragraphs-builder')
    let data = [];
    data.push(element.getAttribute('data-entity-type'));
    data.push(element.getAttribute('data-entity-id'));
    data.push(element.getAttribute('data-field'));
    return data;
}

const buildAdditionalParameters = (url: string, parameters: GenericObject) => {
    let urlWithArguments = url;
    if (keys(parameters).length > 0) {
        let args = map(parameters, (value, key) => `&${key}=${value}`).join('');
        urlWithArguments = `${urlWithArguments}${args}`;
    }

    return urlWithArguments;
}


export const fetchData = (operation: string, parameters: GenericObject = {}) => {
    let routedUrl = buildUrl(mainResourceGetUri, getEntityData());
    console.log(routedUrl);
    routedUrl = buildAdditionalParameters(routedUrl, { operation, ...parameters });
    return Axios.get(getHost() + routedUrl);
}

export const postData = (operation: string, body: any) => {
    const routedUrl = buildUrl(mainResourcePostUri, [...getEntityData(), operation]);
    console.log(token);
    return Axios.post(getHost() + routedUrl, body, {
        headers: {
            'X-CSRF-Token': token
        }
    });
}

export const registerComponent = (component: ParagraphFieldComponent) => {
    if (component.types.length && component.name && typeof component.function === "function") {
        // Check if there is any component with the same type.
        // const existingComponent = window.PB.components.find(com => com.type === component.type);
        // if (existingComponent) {
        //     // Warn the user.
        //     console.warn("There are two components with the same type. This could lead to unpredictable results.");
        // }

        window.PB.components.push(component);
    } else {
        throw new Error("This component is not valid.");
    }
}

export const getFieldComponent = (type: string) => {
    const component = window.PB.components.find(component => component.types.find(typeInArray => typeInArray === type));
    if (component) {
        return component.function;
    } else {
        return null;
    }
}
