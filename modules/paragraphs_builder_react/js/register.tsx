import { IParagraphsBuilderGlobal } from 'interfaces';
import { registerComponent } from 'helper';

declare global {
    interface Window {
        PB: IParagraphsBuilderGlobal
    }
}
// Initalize PB components.
window.PB = {};
window.PB.registerComponent = registerComponent;
window.PB.components = [];


import 'components/general/Fields/TextField';
import 'components/general/Fields/TextArea';