import ReactDOM from 'react-dom';
import React, { useState, useCallback, useContext } from 'react';
import $ from 'jquery';
import Modal from 'components/general/Modal';
import { EntityContext } from 'ParagraphsBuilder';

$('body').append('<div id="PB-Modal-Container"></div>');
const modalContainer = document.getElementById("PB-Modal-Container");


export const useModal = (title: string) => {
    const [show, setToggle] = useState(false);
    const [content, setContent] = useState(null);

    const handleClose = useCallback(() => {
        setToggle(false);
    }, [show]);

    const render = ReactDOM.createPortal(<Modal onClose={handleClose} title={title} show={show}>{content}</Modal>, modalContainer);
    return { render, setToggle, setContent };
}

export const useEntity = () => {
    return useContext(EntityContext);
}