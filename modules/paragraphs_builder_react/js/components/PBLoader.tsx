import React from 'react';
import { map } from 'lodash';

interface PBLoaderProps<T> {
    items: T[],
    children: (items: T, index: string | number) => JSX.Element
}

const PBLoader = <T extends any>({ items, children }: PBLoaderProps<T>) => {
    return <>{items === null ? "Loading..." : map(items, (item, index) => children(item, index))}</>
}

export default PBLoader
