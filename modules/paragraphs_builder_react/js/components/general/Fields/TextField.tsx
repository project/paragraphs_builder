import React from 'react'
import { ParagraphFieldFunction } from 'interfaces'

interface TextFieldProps extends ParagraphFieldFunction {

}

const TextField = ({ type, label, formRef, name }: TextFieldProps) => {
    return <input type="text" name={name} ref={formRef} />
}

window.PB.registerComponent({
    types: ["text"],
    name: "TextField",
    function: TextField
});