import React from 'react'
import { ParagraphFieldFunction } from 'interfaces'

const TextArea = (props: ParagraphFieldFunction) => {
    return <textarea ref={props.formRef} name={props.name} />
}

export default TextArea

window.PB.registerComponent({
    types: ["text_textarea"],
    name: "Textarea",
    function: TextArea
});