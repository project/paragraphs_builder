import React from 'react'
import { CSSTransition } from 'react-transition-group'
import './style.scss';
import { Close } from '@material-ui/icons';
interface ModalProps {
    title: string,
    children: JSX.Element | JSX.Element[],
    show: boolean,
    onClose?: () => void,
}

const Modal = ({ title, children, show, onClose }: ModalProps) => {
    return (
        <CSSTransition in={show} timeout={500} classNames={"modal"} mountOnEnter unmountOnExit>
            <div className="PB-Modal">
                <div className="PB-Modal-Overlay" />
                <div className="PB-Modal-Content">
                    <div className="PB-Modal-Header">
                        <h2>{title}</h2>
                        <a className="PB-Modal-Close" onClick={onClose}><Close /></a>
                    </div>
                    <div className="PB-Modal-Body">
                        {children}
                    </div>
                </div>
            </div>
        </CSSTransition>
    )
}

export default Modal
