import React, { ButtonHTMLAttributes } from 'react'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    value: string;
}

const Button = ({ type = "button", className = "button", ...other }: ButtonProps) => {
    const classes = [className];
    if (type === "submit") {
        classes.push('form-submit');
    }
    return <input type={type} className={className} {...other} />
}

export default Button
