import React, { useContext } from 'react'
import { ParagraphsContext } from 'ParagraphsBuilder'
import PBLoader from './PBLoader';
import Paragraph from './PBContainer/Paragraph';
import { useDrop } from 'react-dnd';
import Button from './general/Button';

const PBContainer = () => {
    const { paragraphs, requestSaveParagraphs } = useContext(ParagraphsContext);
    return (
        <div className="PB-Container">
            <PBLoader items={paragraphs}>
                {(item, index: number) => <Paragraph key={item.id} index={index} id={item.id} html={item.html} paragraphType={item.paragraphType} />}
            </PBLoader>
            <div className="PB-Paragraphs-Form">
                <Button type="submit" value="Test" onClick={requestSaveParagraphs} />
            </div>
        </div>
    )
}

export default PBContainer
