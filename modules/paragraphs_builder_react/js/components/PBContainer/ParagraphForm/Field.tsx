import React, { ReactComponentElement } from 'react'
import { ParagraphField } from 'interfaces'
import { getFieldComponent } from 'helper';
import { ValidationOptions } from 'react-hook-form';

interface FieldProps extends ParagraphField {
    register: (options: ValidationOptions) => React.Ref<HTMLInputElement>
}

const Field = (props: FieldProps) => {
    const { type, label } = props;
    const FieldComponent = getFieldComponent(type);
    return (
        <div className="PB-Form-Field">
            <label className="PB-Form-Label">{label}</label>
            {FieldComponent ? <FieldComponent name={props.name} formRef={props.register({ required: props.required })} {...props} /> : <div>Component "{type}" not found!</div>}
        </div>
    )
}

export default Field
