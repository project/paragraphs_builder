import React, { useEffect, useContext, useState, useCallback } from 'react'
import { useForm } from 'react-hook-form';
import { ParagraphField, FormDisplay } from 'interfaces';
import Field from './Field';
import { fetchData, getFieldComponent } from 'helper';
import { GET, OPERATIONS } from 'api';
import { ParagraphsContext } from 'ParagraphsBuilder';
import { useEntity } from 'hooks';
import PBLoader from 'components/PBLoader';
import { map } from 'lodash';
import Button from 'components/general/Button';

interface ParagraphFormProps {
    onSubmit: (data: any) => void
    paragraphType: string
    index: number
}

const ParagraphForm = ({ onSubmit, paragraphType, index }: ParagraphFormProps) => {
    const { register, handleSubmit, errors } = useForm({
        defaultValues: {
            type: paragraphType
        }
    });
    const [formData, setFormData] = useState<FormDisplay[]>(null);
    // Call the form api.
    useEffect(() => {
        const fetch = async () => {
            const data = await fetchData(OPERATIONS.GET.PARAGRAPHS_FORM, { paragraphType });
            setFormData(data.data);
        }   

        fetch();
    }, []);

    const renderForm = useCallback(() => {
        const fields = map(formData, (fieldData, fieldName: string) =>
            <Field register={register} type={fieldData.type} label={fieldData.label} name={fieldName} required={fieldData.required} />
        );
        return <>
            {/* Hidden field to store the paragraph type. */}
            <input type="hidden" name="type" value={paragraphType} ref={register()} />
            {/* Render the fields */}
            {fields}
            {/* Submit button */}
            <Button value="Submit" type="submit" />
        </>;
    }, [formData]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            {renderForm()}
        </form>
    )
}

export default ParagraphForm
