import React, { useRef, useContext } from 'react'
import { useDrop, useDrag } from 'react-dnd'
import { TYPES } from 'helper';
import { IParagraphTypeDragItem, IParagraph, IParagraphDragItem } from 'interfaces';
import { ParagraphsContext } from 'ParagraphsBuilder';

interface ParagraphsProps extends IParagraph {
    index: number
}

const Paragraph = ({ id, paragraphType, html, index }: ParagraphsProps) => {
    const ref = useRef(null);
    const { paragraphs, createEmptyParagraph, moveParagraph, addParagraph } = useContext(ParagraphsContext);
    const [, dropRef] = useDrop({
        accept: [TYPES.PARAGRAPH_TYPE, TYPES.PARAGRAPH],
        hover: (item: (IParagraphDragItem | IParagraphTypeDragItem), monitor) => {
            if (!ref.current) {
                return;
            }

            if (item.type == TYPES.PARAGRAPH_TYPE && typeof item.id === "string") {
                if (!id) {
                    return;
                }

                createEmptyParagraph(item.id, index);
                item.index = index;
            }

            if (item.type == TYPES.PARAGRAPH && "index" in item && index != item.index) {
                moveParagraph(id, item.index, index);
                item.index = index;
            }
        },
        drop: (item, monitor) => {
            if (item.type == TYPES.PARAGRAPH_TYPE && typeof item.id === "string") {
                addParagraph(item.id, item.index);
            }
        }
    });

    const [, dragRef] = useDrag({
        item: { id, paragraphType, index, type: TYPES.PARAGRAPH }
    });

    dropRef(dragRef((ref)));
    return id ? (
        <article data-index={index} className="PB-Paragraph-Item" data-paragraph-id={id} data-paragraph-type={paragraphType} dangerouslySetInnerHTML={{ __html: html }} ref={ref} />
    ) : <div ref={ref} data-index={index}>Create a "{paragraphType}" here.<div>Test</div><div>Best</div></div>
}

export default Paragraph
