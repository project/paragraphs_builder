import React, { useCallback, useContext } from 'react'
import { ToolbarContext } from '../PBToolbar';

interface ToolbarTrayProps {
    children: JSX.Element | JSX.Element[] | string,
    index: number,
    width?: number
}

const ToolbarTray = ({ children, index, width }: ToolbarTrayProps) => {

    const { activeIndex } = useContext(ToolbarContext);
    const isActive = activeIndex === index;

    const getClassnames = useCallback(() => {
        const classNames = ['PB-Toolbar-Tray PB-Fixed'];
        if (isActive) {
            classNames.push("PB-Active");
        }
        return classNames.join(' ');
    }, [activeIndex]);

    return (
        <div className={getClassnames()} style={{ width, left: isActive ? "100%" : -width }}>
            {children}
        </div>
    )
}

export default ToolbarTray
