
import React, { useContext, useCallback } from 'react';
import { SvgIconComponent } from '@material-ui/icons';
import { ToolbarContext } from '../PBToolbar';

export interface ToolbarItemProps {
    name: string,
    icon: SvgIconComponent,
    index?: number
}

export const ToolbarItem = ({ name, icon, index }: ToolbarItemProps) => {
    const Icon = icon;
    const { setActiveIndex, activeIndex } = useContext(ToolbarContext);

    const handleItemClick = useCallback(() => {
        setActiveIndex(index === activeIndex ? null : index);
    }, [activeIndex]);

    const getClassname = useCallback(() => {
        const classNames = ['PB-Toolbar-Link'];
        if (index === activeIndex) {
            classNames.push('PB-Active');
        }

        return classNames.join(' ');
    }, [activeIndex]);

    return (
        <li className="PB-Toolbar-Item">
            <a className={getClassname()} title={name} onClick={handleItemClick}>
                <Icon />
            </a>
        </li>
    )
}

