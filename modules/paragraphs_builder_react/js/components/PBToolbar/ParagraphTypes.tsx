import React from 'react'
import { IParagraphType } from '../../interfaces';
import ParagraphTypeItem from './ParagraphTypeItem';

interface ParagraphTypesProps {
    paragraphTypes: IParagraphType[]
}

const ParagraphTypes = ({ paragraphTypes }: ParagraphTypesProps) => {

    return <>{paragraphTypes.map(paragraphType => <ParagraphTypeItem key={paragraphType.id} src={paragraphType.src} name={paragraphType.name} id={paragraphType.id} />)}</>
}

export default ParagraphTypes
