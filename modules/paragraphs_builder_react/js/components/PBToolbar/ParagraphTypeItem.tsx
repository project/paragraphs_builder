import React from 'react'
import { IParagraphType, IParagraph } from 'interfaces'
import { useDrag } from 'react-dnd';
import { TYPES } from 'helper';

const ParagraphTypeItem = ({ name, id, src }: IParagraphType) => {
    const [{ }, dragRef] = useDrag({
        item: { name, id, type: TYPES.PARAGRAPH_TYPE }
    });
    return (
        <a className="PB-ParagraphType-Item" title={name} ref={dragRef}>
            <img title={name} alt={name} src={src} />
        </a>
    )
}

export default ParagraphTypeItem
