import React from 'react';
import { ToolbarItem, ToolbarItemProps } from './ToolbarItem';

interface ToolbarItemsProps {
    items: Array<ToolbarItemProps>
}
const ToolbarItems = ({ items }: ToolbarItemsProps) => {
    return <ul className="PB-Toolbar-Items">{items.map((item, index) => <ToolbarItem key={index} name={item.name} icon={item.icon} index={index} />)}</ul>;
}

export default ToolbarItems;