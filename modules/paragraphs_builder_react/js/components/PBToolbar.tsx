import React, { createContext, useState, useEffect } from 'react'
import ToolbarItems from './PBToolbar/ToolbarItems';
import { ToolbarItemProps } from './PBToolbar/ToolbarItem';
import ToolbarTray from './PBToolbar/ToolbarTray';
import './style.scss';
import Paragraphs from './PBToolbar/ParagraphTypes';
import { IParagraphType } from 'interfaces';

interface PBToolbarProps {
    items: ToolbarItemProps[],
    paragraphTypes: IParagraphType[]
}
interface IToolbarContext {
    activeIndex: number,
    setActiveIndex: (index: number) => void
}

export const ToolbarContext = createContext<IToolbarContext>(null);

const PBToolbar = ({ items, paragraphTypes }: PBToolbarProps) => {

    const [activeIndex, setActiveIndex] = useState(null);

    return (
        <div id="PB-Toolbar" className="PB-Toolbar-Container PB-Fixed">
            <ToolbarContext.Provider value={{ activeIndex, setActiveIndex }}>
                <div className="PB-Toolbar">
                    <ToolbarItems items={items} />
                </div>
                <ToolbarTray width={150} index={0}>
                    <Paragraphs paragraphTypes={paragraphTypes} />
                </ToolbarTray>
            </ToolbarContext.Provider>
        </div>
    )
}






export default PBToolbar
