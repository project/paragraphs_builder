export const mainResourceGetUri = '/paragraphs_builder/%/%/%';
export const mainResourcePostUri = '/paragraphs_builder/%/%/%/%';

export const GET = {
    PARAGRAPH_TYPES: '/paragraphs_builder/paragraph_types/get',
    PARAGRAPHS: '/paragraphs_builder/%/%/%/paragraphs/get',
    PARAGRAPHS_FORM: '/paragraphs_builder/paragraphs/%/form'
}

export const POST = {
    SAVE_PARAGRPAHS: "/paragraphs_builder/%/%/%/paragraphs/save",
    CREATE_PARAGRAPH: "/paragraphs_builder/%/%/%/%/paragraphs/create"
}

export const OPERATIONS = {
    GET: {
        PARAGRAPH_TYPES: 'getParagraphTypes',
        PARAGRAPHS: 'getParagraphs',
        PARAGRAPHS_FORM: 'getParagraphForm'
    },
    POST: {
        SAVE_PARAGRAPHS: 'saveParagraphs'
    }
}