import React, { useEffect, useState, createContext, useCallback } from 'react'
import PBToolbar from './components/PBToolbar'
import { ToolbarItemProps } from './components/PBToolbar/ToolbarItem';
import { Add } from '@material-ui/icons';
import './style.scss';
import { fetchData, postData, getHost } from 'helper';
import { GET, POST, OPERATIONS } from 'api';
import { IParagraph, IParagraphsContext, ParagraphFieldComponent, ParagraphField, IFieldableEntity } from 'interfaces';
import PBContainer from 'components/PBContainer';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend'
import { useModal } from 'hooks';
import ParagraphForm from 'components/PBContainer/ParagraphForm';
import { omit } from 'lodash';

const toolbarItems: ToolbarItemProps[] = [
    { name: "Add Paragraph", icon: Add }
];




export const ParagraphsContext = createContext<IParagraphsContext>(null);
export const EntityContext = createContext<IFieldableEntity>(null);

export const ParagraphsBuilder = ({ entityType, entityId, field }: IFieldableEntity) => {

    const [paragraphTypes, setParagraphTypes] = useState([]);
    const [paragraphs, setParagraphs] = useState<IParagraph[]>(null);
    const { render, setToggle, setContent } = useModal("Create Paragraph");

    // Fetch all paragraph types.
    useEffect(() => {
        const fetch = async () => {
            const res = await fetchData(OPERATIONS.GET.PARAGRAPH_TYPES);
            setParagraphTypes(res.data);
        }

        fetch();
    }, []);

    useEffect(() => {
        const fetch = async () => {
            const res = await fetchData(OPERATIONS.GET.PARAGRAPHS);
            setParagraphs(res.data);
        }

        fetch();
    }, []);

    const exitParagraphsBuilder = useCallback(() => {
        window.location.href = getHost() + `/${entityType}/${entityId}`;
    }, []);

    const createEmptyParagraph = useCallback((type: string, index: number) => {
        // Create an empty paragraph.
        const emptyParagraph: IParagraph = { id: null, html: null, paragraphType: type };
        // Copy the paragraphs.
        let newParagraphs = [...paragraphs];
        // Remove any empty paragraph first.
        newParagraphs = newParagraphs.filter(paragraph => paragraph.id != null);
        // Insert at.
        newParagraphs.splice(index, 0, emptyParagraph);
        // Set the paragraphs.
        setParagraphs(newParagraphs);
    }, [paragraphs]);

    const moveParagraph = useCallback((id: number, oldIndex: number, newIndex: number) => {
        let newParagraphs = [...paragraphs];
        // Save the paragraph first.
        const movedParagraph = newParagraphs[oldIndex];
        // Remove the paragraph.
        newParagraphs.splice(oldIndex, 1);
        // Insert it at index.
        newParagraphs.splice(newIndex, 0, movedParagraph);
        // Save.
        setParagraphs(newParagraphs);
    }, [paragraphs]);

    const handleFormSubmit = async (data: any) => {
        // Post the data.
        const type = data.type;
        const newData = omit(data, 'type');
        const res = await postData(POST.CREATE_PARAGRAPH, newData, [entityType, entityId, field, type]);

        // Set new paragraphs.
        const newParagraphs = [...paragraphs];
        const index = paragraphs.findIndex(paragraph => paragraph.id == null);
        newParagraphs.splice(index, 1, res.data);
        setParagraphs(newParagraphs);

        // Hide the modal.
        setToggle(false);
    }

    const addParagraph = useCallback((paragraphType: string, index: number) => {
        const newParagraphs = [...paragraphs];
        setParagraphs(newParagraphs);
        setContent(<ParagraphForm onSubmit={handleFormSubmit} paragraphType={paragraphType} index={index} />)
        setToggle(true);
    }, [paragraphs]);

    const requestSaveParagraphs = useCallback(async () => {
        // Initialize the values of paragraphs.
        const paragraphsToPost = paragraphs.map(paragraph => ({
            target_id: paragraph.id,
            target_revision_id: paragraph.revision_id
        }));

        await postData(OPERATIONS.POST.SAVE_PARAGRAPHS, paragraphsToPost);

        exitParagraphsBuilder();
    }, [paragraphs]);

    return (
        <DndProvider backend={HTML5Backend}>
            <EntityContext.Provider value={{ entityType, entityId, field }}>
                <ParagraphsContext.Provider value={{ paragraphs, setParagraphs, createEmptyParagraph, moveParagraph, addParagraph, requestSaveParagraphs }}>
                    <PBToolbar items={toolbarItems} paragraphTypes={paragraphTypes} />
                    <PBContainer />
                    {render}
                </ParagraphsContext.Provider>
            </EntityContext.Provider>
        </DndProvider>

    )
}
