import { ParagraphsBuilder } from './ParagraphsBuilder';
import React from 'react';
import ReactDOM from 'react-dom';
import 'register';
import Axios from 'axios';
import { getHost, fetchData } from 'helper';

declare var Drupal: {
    t: (stringToTranslate: string) => string
}

const container = document.getElementById('paragraphs-builder');
const entityType = container.getAttribute('data-entity-type');
const entityId = container.getAttribute('data-entity-id');
const field = container.getAttribute('data-field');

ReactDOM.render(<ParagraphsBuilder entityType={entityType} entityId={entityId} field={field} />, container);