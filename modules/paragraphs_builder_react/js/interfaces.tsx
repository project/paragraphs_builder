import { DragObjectWithType } from "react-dnd";
import { LegacyRef, Ref } from "react";

export interface IParagraphType {
    name: string,
    id: string,
    src: string
}

export interface IParagraphTypeDragItem extends DragObjectWithType, IParagraphType {
    index?: number
}

export interface IParagraph {
    id: number,
    paragraphType: string,
    html: string,
    revision_id?: number
}

export interface IParagraphDragItem extends DragObjectWithType, IParagraph {
    index?: number
}


export interface IParagraphsContext {
    paragraphs: IParagraph[],
    setParagraphs: (paragraphs: IParagraph[]) => void,
    createEmptyParagraph: (type: string, index: number) => void;
    moveParagraph: (id: number, oldIndex: number, newIndex: number) => void;
    addParagraph: (paragraphType: string, index: number) => void;
    requestSaveParagraphs: () => void
}

export interface IFieldableEntity {
    entityType: string,
    entityId: string,
    field: string
}

export interface ParagraphField {
    type: string,
    label: string,
    required: boolean,
    name: string
}

export interface IParagraphsBuilderGlobal {
    components?: ParagraphFieldComponent[];
    registerComponent?: (component: ParagraphFieldComponent) => void
}

export type ParagraphFieldComponent = {
    types: string[];
    name: string;
    function: React.FC<ParagraphFieldFunction>
}

export interface ParagraphFieldFunction extends ParagraphField {
    name: string,
    formRef: Ref<any> & LegacyRef<any>
}

export interface FormDisplay {
    weight: number;
    settings: Record<string, any>;
    third_party_settings: Record<string, any>;
    type: string;
    region: string;
    label: string;
    required: boolean;
}

export type GenericObject = Record<string, string | number>;