<?php

namespace Drupal\paragraphs_builder_react\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\paragraphs_builder_api\Controller\ParagraphsBuilderControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ParagraphsBuilderReactController.
 */
class ParagraphsBuilderReactController extends ParagraphsBuilderControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * Provides an interface to use paragraphs builder.
   *
   * @return \Drupal\Core\Render\Markup
   *   Returns the markup of the paragraphs builder.
   */
  public function build($entity_type, $entity_id, $field) {
    if ($this->checkParagraphFieldInEntity($entity_type, $entity_id, $field)) {
      return [
        'app' => [
          '#type' => 'container',
          /** @todo Use a twig theme */
          '#attributes' => [
            'id' => 'paragraphs-builder',
            'class' => ['Paragraphs-Builder'],
            'data-entity-type' => $entity_type,
            'data-entity-id' => $entity_id,
            'data-field' => $field,
            'data-host' => $this->request->getSchemeAndHttpHost(),
          ],
          '#cache' => [
            'max-age' => Cache::PERMANENT,
          ],
          '#attached' => [
            'library' => ['paragraphs_builder_react/app'],
          ],

        ],
        'livereload' => [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#attributes' => [
            'src' => 'http://localhost:35729/livereload.js',
          ],
        ],
      ];
    } else {
      throw new NotFoundHttpException();
    }
  }

}
